//O FORM CREATE é usado para criar e editar usuários para qualquer  perfil

import React, { useState, useEffect } from 'react'
import { patchByUserId, getByIdAdnByRoute, GetAllFromRoute } from '../../services/requisicoes'
import { useHistory, useParams } from 'react-router-dom'
import Alert from '../alert/index'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'
import { Button, FormGroup, Label, Input, Col } from 'reactstrap';
import { Container, Form } from 'react-bootstrap'
import Loading from '../loading/loading'
import styled from 'styled-components'

const UserCreate = (props) => {

    const [listExercicios, setListExercicios] = useState([])

    /**setSelectedExercices não é chamado mas selectedExercices é usado pelo array */
    const [selectedExercices, setSelectedExercices] = useState([])

    const { id } = useParams()
    const [alert, setAlert] = useState({})
    const history = useHistory()
    const [isEdit, setisEdit] = useState(false)
    const [userFunction, setUserFunction] = useState("ALUNO")

    const [form, setForm] = useState({
        funcao: ""
    })


  
     
    const getList = async () => {
        try {

            const usersTodos =    await GetAllFromRoute('exercises')

            if (usersTodos) { setListExercicios(sortList(usersTodos.data)) }

        } catch (error) {
            alert("Houve um erro na busca da lista de exercícios. Favor contactar o suporte ao usuário")
        }
    }

    const sortList = (users) => {
        return users.sort((a, b) => {
            if (a.name < b.name) {  return -1  }
            if (a.name > b.name) {  return 1 }
            return 0;
        })   }


    useEffect(() => {
        getList()
        return () => { }
    })



    //Este usereffet mostra o usuário logado 
    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken())
            setUserFunction(user.funcao)
        })()
        return () => { }

    }, [])

    useEffect(() => {

        //esta function exibe o user em edição
        const getShowUser = async () => {
            const user = await getByIdAdnByRoute('list', id)
            setForm(user.data)
        }

        if (id) {
            setisEdit(true)            
            getShowUser()
        }

    }, [id])



    const handleChange = (event) => {

        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;

          setForm({
            ...form,
            [name]: value
        });
    }


    const  manipulaExercicios = (event) => {
        const target = event.target;
        var value = target.value;
        var newline = String.fromCharCode(13, 10);

         
        if(target.checked){
            selectedExercices.push(value + newline) //  [value] = value;   
        }else{
            var a = selectedExercices.indexOf(value + newline);
            selectedExercices.splice(a, 1);
        }
        
        setForm({
            ...form,
            exercicios: selectedExercices.toString()
        });
    }


    const formIsValid = () => {
       return form.exercicios 
    }


    const submitForm = async (event) => {
        try {


            setForm({
                exercicios: form.exercicios.toString()
            })


            await patchByUserId(id, form)

            setForm({
                          })


            setAlert({
                type: "success",
                message: 'Seu formulário foi enviado com sucesso',
                show: true
            })



            setTimeout(function () {

                if (userFunction.includes("ADM")) {
                    history.push("/list")
                }

                if (userFunction.includes("PROFESSOR")) {
                    history.push("/professorlist")
                }

            }, 1000)

        } catch (e) {
            setAlert({
                type: "error",
                message: 'Ocorreu um erro no cadastro',
                show: true
            })

        }

    }

    const cancelEdit = () => {

        history.push("/list")

    }



    const ProvessorForm = (
        <div>


            <FormGroup row>
                <Label htmlFor="auth_tel" sm={2}>Aluno:</Label>
                <Col sm={10}>
                    <Input
                        disabled
                        type="text"
                        id="auth_name"
                        name="name"
                        value={form.name || ""}
                    />
                </Col>
            </FormGroup>

            <FormGroup row>
                <Label htmlFor="auth_tel" sm={2}>Série:</Label>
                <Col sm={10}>
                    <Input
                        type="textarea"
                        id="auth_exercicios"
                        name="exercicios"
                        onChange={handleChange}
                        value={form.exercicios || ""}
                        placeholder="Série" />
                </Col>
            </FormGroup>

            <FormGroup row>

                <Label htmlFor="auth_tel" sm={2}>Seleção de exercícios :</Label>


                <Col sm={10}>

{listExercicios.length <= 0 ? <Loading show={true} /> : ""}
                    <div className="scroll col-sm">

                        
                    
                            {listExercicios.map((type, index) => (
                                <div key={index} className="oeste">


                                    <Form.Check type='checkbox' id={`check-api-checkbox`}>
                                        <Form.Check.Input type="checkbox" isValid  id={`checkId ${type}${index}`} onChange={manipulaExercicios} value={type.name}    />
                                        <Form.Check.Label>{` ${type.name}`} </Form.Check.Label>
                                    </Form.Check>


                                </div>
                            ))}

                    
                    </div>


                </Col>


            </FormGroup>

            


            <FormGroup row>
                <Label htmlFor="auth_tel" sm={2}>Avaliação Física :</Label>
                <Col sm={10}>
                    <Input
                        type="textarea"
                        id="auth_avaliacao"
                        name="avaliacao"
                        onChange={handleChange}
                        value={form.avaliacao || ""}
                        placeholder="Comente a avaliação física do Aluno" />
                </Col>
            </FormGroup>
        </div>
    )



 
    return (

        <Container>
            <div className='centro'>

                <div className="showMyDiv vh50">
                    <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false} />

                    {ProvessorForm}

                    <Button2 className="orange_dark" disabled={!formIsValid()} onClick={submitForm}>{(isEdit ? 'Alterar' : 'Cadastrar')} </Button2>
                    <Button2 className="orange_dark" onClick={cancelEdit}>Cancelar</Button2>
                   

                </div>
            </div>
        </Container>
    )
}

export default UserCreate


const Button2 = styled(Button)`
background-color: rgb(2, 4, 56);
border: solid 1px rgb(2, 4, 56);
color:rgb(95, 126, 128);
`