
import React, { useState, useRef, useEffect } from 'react'
import { createUser, getByIdAdnByRoute, updateUser } from '../../services/requisicoes'
import { useHistory, useParams } from 'react-router-dom'
import Alert from '../alert/index'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'
import { Button, FormGroup, Label, Input, Col } from 'reactstrap';
import { Container } from 'react-bootstrap'


const UserCreate = (props) => {

    const inputName = useRef(null);
    const inputEmail = useRef(null);
    const { id } = useParams()
    const [alert, setAlert] = useState({})
    const history = useHistory()
    const [isEdit, setisEdit] = useState(false)

    const [disableProfessor, setdisableProfessor] = useState(true)

    const [userFunction, setUserFunction] = useState("ALUNO")
    const methodUser = isEdit ? updateUser : createUser

    const [form, setForm] = useState({
        funcao: ""
    })



    //Este usereffet mostra o usuário logado 
    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken())
            setUserFunction(user.funcao)
        })()
        return () => { }

    }, [])

    useEffect(() => {

        //esta function exibe o use em edição
        const getShowUser = async () => {
            const user = await getByIdAdnByRoute('list', id)
            
            setForm(user.data)
        }

        if (id) {
            setisEdit(true)
            setdisableProfessor(false)
            getShowUser()
            
        }

    }, [id])



    const handleChange = (event) => {

        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;

        if (event.target.type === 'select-one') {
            if (event.target.value === 'ALUNO') {
                setdisableProfessor(false)

            } else {
                setdisableProfessor(true)

                form.professor = ""
            }
        }



        setForm({
            ...form,
            [name]: value
        });
    }


    const formIsValid = () => {

        if (form.funcao.includes("ALUNO") && !id ) {
            return form.name && form.telefone && form.password && form.funcao && form.login && form.mensalidade && form.professor
        } else {
           return form.name && form.telefone && form.password && form.funcao && form.login 
        }

    }


    const submitForm = async (event) => {
        try {

            await methodUser(form)
            setForm({
                ...form,
                is_admin: false,
            })


            setAlert({
                type: "success",
                message: 'Seu formulário foi enviado com sucesso',
                show: true
            })


            setTimeout(function () {

                if (userFunction.includes("ADM")) {
                    history.push("/list")
                }

                if (userFunction.includes("PROFESSOR")) {
                    history.push("/professorlist")
                }

            }, 1000)

        } catch (e) {
            setAlert({
                type: "error",
                message: 'Ocorreu um erro no cadastro',
                show: true
            })
 
        }

    }

    const cancelEdit = () => {
        history.push("/list")
    }



    const AdminForm1 = (
        <>

            <FormGroup row>
                <Label for="funcao" sm={2}>Funcão:</Label>
                <Col sm={10}>
                    <Input type="select" name="funcao" id="auth_funcao" onChange={handleChange} value={form.funcao || ""}>
                        <option></option>
                        <option>ALUNO</option>
                        <option>ADM</option>
                        <option>PROFESSOR</option>
                        <option>ESTAGIARIO</option>
                    </Input>
                </Col>
            </FormGroup>

            <FormGroup row>
                <Label htmlFor="auth_name" sm={2} >Nome:</Label>
                <Col sm={10}>
                    <Input ref={inputName} type="text" id="auth_name" name="name"
                        onChange={handleChange} value={form.name || ""} placeholder="Insira o nome do usuário" />
                </Col>
            </FormGroup>

            <FormGroup row>
                <Label htmlFor="auth_tel" sm={2}>Telefone:</Label>
                <Col sm={10}>
                    <Input ref={inputEmail}
                        type="number"
                        id="auth_tel"
                        name="telefone"
                        onChange={handleChange}
                        value={form.telefone || ""}
                        placeholder="Telefone" />
                </Col>
            </FormGroup>

            <FormGroup row>
                <Label htmlFor="auth_login" sm={2}>Login:</Label>
                <Col sm={10}>
                    <Input
                        type="text"
                        id="auth_login"
                        name="login"
                        onChange={handleChange}
                        value={form.login || ""}
                        placeholder="login" />
                </Col>
            </FormGroup>


            <FormGroup row>
                <Label htmlFor="auth_password" sm={2}>Senha:</Label>
                <Col sm={10}>
                    <Input
                        autoComplete="new-password"
                        type="password"
                        id="auth_password"
                        name="password"
                        onChange={handleChange}
                        value={form.password || ""}
                        placeholder="password" />
                </Col>
            </FormGroup>





            {!form.funcao.includes("ALUNO")  ? "" :
                <FormGroup row>
                    <Label htmlFor="auth_professor" sm={2}  >Professor:</Label>
                    <Col sm={10}>
                        <Input
                            type="text"
                            id="auth_professor"
                            name="professor"
                            onChange={handleChange}
                            value={form.professor || ""}
                            placeholder="Insira o nome do professor escolhido para o aluno."
                            disabled={disableProfessor} />
                    </Col>
                </FormGroup>
            }

            {!form.funcao.includes("ALUNO") ? "" :
                <FormGroup row>
                    <Label htmlFor="auth_mensalidade" sm={2}>Vencimento Mensalidade</Label>
                    <Col sm={10}>
                        <Input
                            type="date"
                            id="auth_mensalidade"
                            name="mensalidade"
                            onChange={handleChange}
                            value={form.mensalidade || ""}
                            placeholder="" />
                    </Col>
                </FormGroup>
            }


            <FormGroup row>
                <Label for="funcao" sm={2}>Usuário ativo?</Label>
                <Col sm={10}>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Input type="checkbox" name="is_active" onChange={handleChange} id="is_active" checked={form.is_active || false} />
                </Col>
            </FormGroup>




        </>
    )
    return (

        <Container>
            <div className='centro'>
                <div className="showMyDiv vh50">
                    <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false} />
                     {AdminForm1 }
                    <Button className="orange_dark" disabled={!formIsValid()} onClick={submitForm}>{(isEdit ? 'Alterar' : 'Cadastrar')} </Button>
                    <Button className="orange_dark" onClick={cancelEdit}>Cancelar</Button>
                </div>
            </div>
        </Container>
    )
}

export default UserCreate
