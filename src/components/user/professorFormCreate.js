import React, { useEffect, useState } from 'react'
import { FaTrashAlt } from 'react-icons/fa'
import { Button, Col, Container, FormGroup, Input, Label, Table } from 'reactstrap'
import styled from 'styled-components'
import { GetAllFromRoute, deleteByRouteAndId, PostInRoute } from '../../services/requisicoes'
import Loading from '../loading/loading'

const ProfessorFormCreate = () => {

    const [listExercicios, setListExercicios] = useState([])

    const [loading, setloading] = useState(false)

    const [form, setForm] = useState({
        name: ""
    })
    
    const getList = async () => {
        try {
            setloading(true)
          
            const usersTodos =    await GetAllFromRoute('exercises')

            if (usersTodos) { 
                setListExercicios(sortList(usersTodos.data)) 
            setloading(false)
            }
            
        } catch (error) {
            setloading(false)
        }
    }


    

    const postNewExercicio = async () => {
        try {

            await PostInRoute('exercises', form)
            setForm({})
            getList()

            alert("Cadastrado")
        } catch (error) {
            setloading(false)
        }
    }

    const sortList = (users) => {
        return users.sort((a, b) => {
            if (a.name < b.name) {  return -1  }
            if (a.name > b.name) {  return 1 }
            return 0;
        })   }


    useEffect(() => {
        getList()
        return () => { }
    })

    const deleteExercicio = async (exercicio, e) =>{

        try {
            await deleteByRouteAndId('exercises',exercicio._id )
            getList()
            alert(exercicio.name + " deletado")
        } catch (error) {
            setloading(false)
        }
    }

    
    const handleChange = (event) => {

        const value = event.target.value;
        const name = event.target.name;

          setForm({
            ...form,
            [name]: value
        });
    }


    return (
        <Container>
            <br />

            <FormGroup row>
                <Label htmlFor="auth_tel" sm={1}>Exercício:</Label>
                <Col sm={8}>
                    <Input                        
                        type="text"
                        id="auth_name"
                        name="name"
                        value={form.name||""}
                        onChange={handleChange}
                    />
                </Col>
                <Col sm={3}> 
                <Button2 className="orange_dark" onClick ={postNewExercicio} >Cadastrar</Button2>
                </Col>
            </FormGroup>

            <NewTable striped hover size="sm">
                <thead>
                    <tr>
                        <THeadItem>Exercícios cadastrados</THeadItem>
                        <THeadItem>Ações</THeadItem>

                    </tr>
                </thead>
                <tbody>
                    {listExercicios.map((catg, i) => (
                        <tr key={i}>
                            <TbodyItem>{catg.name} </TbodyItem>
                            <TbodyItem> <span onClick={(e) => deleteExercicio(catg, e)}> <FaTrashAlt />  Excluir </span> </TbodyItem>
                        </tr>
                    ))}

                </tbody>
            </NewTable>
            
            {/* loading enquanto agfuarda a lista de exercicios cadastrados carregar */}
            
            <Loading show={listExercicios.length <=0 ? true : false} /> 
        </Container>
    )
}

export default ProfessorFormCreate



const NewTable = styled(Table)`
    font-size: 14px
`

const THeadItem = styled.th`
    background: #666;
    color:#eee;
`
const TbodyItem = styled.td`

    :nth-child(1){  width: 80%; 
        text-align:left;
    }

    :nth-child(2){  width: 20%;
        cursor:pointer;
    }

`


const Button2 = styled(Button)`
background-color: rgb(2, 4, 56);
border: solid 1px rgb(2, 4, 56);
color:rgb(95, 126, 128);
`