import React, { useEffect, useState } from 'react'
import { ListUserQuery } from '../../services/requisicoes'
import Loading from '../loading/loading'
import { FaPencilAlt } from "react-icons/fa";

import MyModal from '../alert/myModal'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'

import history from '../../config/history'
import styled from 'styled-components';

const ProfessorList = (props) => {


    const [userFunction, setUserFunction] = useState("doidera")
    const [users, setUsers] = useState([]) 
    const [alert, setAlert] = useState({})
    const [loading, setloading] = useState(false)


    const editExercicios = (user) => {

        history.push(`/editexercicios/${user._id || user.id}`)
    }

    const getList = async () => {


        try {
            setloading(true)

            const usersTodos = await ListUserQuery(
                { funcao: "ALUNO" }
            )

            if (usersTodos) {
                setUsers(sortList(usersTodos.data))
            }
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }



    const sortList = (users) => {
        return users.sort((a, b) => {
            if (a.is_active < b.is_active) {
                return 1;
            }
            if (a.is_active > b.is_active) {
                return -1;
            }
            return 0;
        })
    }



    useEffect(() => {
        getList()
        return () => { }
    })





    const estilo = {
        cursor: 'pointer',
        textAlign: 'center'
    }



    //Este usereffet mostra o usuário logado 
    useEffect(() => {
        (async () => {
            const { user: loggedUser } = await jwt.decode(getToken())

            setUserFunction(loggedUser.funcao)

        })()
        return () => { }

    }, [])




    const verifyIsEmpty = users.length === 0



    //**retorna cabeçalho e a lista de user  */
    const cabecalhoLista = () => (

        <>

            <div className="list_user">

                <table className="table table-striped" border="1">
                    <thead>
                        <tr>
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> NOME </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> MATRICULA </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> ID </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> ATIVO </th> : ""}
                            {"ADM".includes(userFunction) ? <th> FUNÇÃO </th> : ""}
                            {"ADM".includes(userFunction) ? <th> TELEFONE </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> AVALIACAO </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> EXERCICIOS </th> : ""}
                            {"ADM_ALUNO".includes(userFunction) ? <th> MENSALIDADE </th> : ""}
                            {"PROFESSOR_ADM_ALUNO".includes(userFunction) ? <th> AÇÕES </th> : ""}

                        </tr>
                    </thead>
                    <tbody>
                        {montarUsuarios()}
                    </tbody>
                </table>
            </div>
        </>
    )


    const adjustExercices = (exer) => {

        var res = exer.split("\r\n,");
        const mont = () => res.map((item, index) => (
            <li key={index}>{item}</li>
        ))

        return (
            <ul >
                {mont()}
            </ul>
        )

    }

    const montarUsuarios = () => users.map((user, index) => (


        <tr key={index} className={user.is_active ? "" : "noActive"}>

            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <TbodyItem> {user.name} </TbodyItem> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <TbodyItem> {user.matricula} </TbodyItem> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <TbodyItem> {user._id || user.id} </TbodyItem> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <TbodyItem> {user.is_active ? "SIM" : "NÃO"} </TbodyItem> : ""}
            {"ADM".includes(userFunction) ? <TbodyItem> {user.funcao} </TbodyItem> : ""}
            {"ADM".includes(userFunction) ? <TbodyItem> {user.telefone} </TbodyItem> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <td> {user.avaliacao} </td> : ""}

            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <TbodyItem> {adjustExercices(user.exercicios)} </TbodyItem> : ""}

            {"ADM_ALUNO".includes(userFunction) ? <TbodyItem> {user.funcao.includes("ALUNO") ? user.mensalidade : ""} </TbodyItem> : ""}

            {"PROFESSOR_ADM_ALUNO".includes(userFunction) ?

                <TbodyItem style={estilo} >

                    {"PROFESSOR".includes(userFunction) ?
                        <span onClick={() => editExercicios(user)} > <FaPencilAlt /> Editar Série</span>
                        : ""}

                </TbodyItem>

                : ""}

        </tr>


    )

    )

    return (
        <>
            <MyModal
                show={alert.show || false}
                title={"Informe:"}
                funcaoFechar={() => setAlert({})}
                messagebody={alert.message || ""}
                simNao={alert.simNao}
                funcaoDesejada={alert.funcaodesejada}
            />

            {!verifyIsEmpty ? cabecalhoLista() : <Loading show={loading} />}
        </>
    )

}




export default ProfessorList

const TbodyItem = styled.td`
    :nth-child(1){  width: 14%; }
    :nth-child(2){  width: 10%; }
    :nth-child(3){  width: 14%; }
    :nth-child(4){  width: 5%; }
    :nth-child(5){  width: 14%; }
    :nth-child(6){  width: 27%; text-align: left;}
    :nth-child(7){  width: 10%; }
`
