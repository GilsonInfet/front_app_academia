import React, { useEffect, useState } from 'react'
import { Container, Table } from 'reactstrap'
import { getByIdAdnByRoute } from '../../services/requisicoes'
import Loading from '../loading/loading'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'


const ViewDeAluno = (props) => {


    const [loading, setloading] = useState(false)
    const [users, setUsers] = useState([])



    useEffect(() => {
        (async () => {
            const { user: loggedUser } = await jwt.decode(getToken())


            if (loggedUser.funcao.includes("ALUNO")) {
                getList(loggedUser.id || loggedUser._id)
            }
            
        })()
        return () => { }

    }, [])


    const getList = async (theid) => {


        try {

            setloading(true)

            const usersTodos = await getByIdAdnByRoute('list', theid)

            if (usersTodos) {
                setUsers(usersTodos.data)
            }

            setloading(false)

        } catch (error) {
            setloading(false)
        }
    }

    const adjustExercices = (exer = "cc\r\n,cc" ) => {

        var res = exer.split("\r\n,");
        const mont = () => res.map((item, index) => (
            <li key={index}>{item}</li>
        ))

        return (
            <ul >
                {mont()}
            </ul>
        )

    }
    
    return (
        <Container>


            <Table striped>

                <thead>
                    <tr>
                        <th>Campos</th>
                        <th>Valor</th>
                    </tr>
                </thead>

                {loading ? <Loading show={loading} /> :

                <tbody>
                    <tr>
                        <td>Nome</td>
                        <td>{users.name}</td>
                    </tr>
                    <tr>
                        <td>Matrícula</td>
                        <td>{users.matricula}</td>
                    </tr>
                    <tr>
                        <td>Vencimento Mensalidade</td>
                        <td>{users.mensalidade}</td>
                    </tr>
                    <tr>
                        <td>Professor</td>
                        <td>{users.professor}</td>
                    </tr>
                    <tr>
                        <td>Exercícios</td>

                        <td  >{adjustExercices(users.exercicios) }</td>
                    </tr>

                    <tr>
                        <td>Avaliação</td>
                        <td>{users.avaliacao || "NÃO CADASTRADO"}</td>
                    </tr>

                </tbody>
                }
            </Table>

        </Container>
    )
}

export default ViewDeAluno
