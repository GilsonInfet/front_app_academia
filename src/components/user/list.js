import React, { useEffect, useState } from 'react'
import {  ListUserQuery, deleteUserById, patcByhRotaAndId } from '../../services/requisicoes'
import Loading from '../loading/loading'
import { FaTrashAlt, FaToggleOn, FaPencilAlt } from "react-icons/fa";

import { IoIosFitness } from "react-icons/io";

import MyModal from '../alert/myModal'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'

import history from '../../config/history'

import { Button, Col, Container, FormGroup, Row } from 'react-bootstrap';

import styled from 'styled-components'

const UserList = (props) => {


    const [userFunction, setUserFunction] = useState("doidera")
    const [users, setUsers] = useState([])
    const [alert, setAlert] = useState({})
    const [loading, setloading] = useState(false)


/** usado em useeffect mas é reclamado como missing dependency : listQuery*/
    const [listQuery, setListQuery] = useState({})

    /**
     * este edit chama push
     * @param {*} user 
     */
    const editUser = (user) => history.push(`/edit/${user._id || user.id}`)

    const editExercicios = (user) => history.push(`/editexercicios/${user._id || user.id}`)

    const getList = async () => {

        try {
            setloading(true)

            const usersTodos = await ListUserQuery(listQuery)

            if (usersTodos) {
                setUsers(sortList(usersTodos.data))
            }
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }



    const sortList = (users) => {
        return users.sort((a, b) => {
            if (a.is_active < b.is_active) {
                return 1;
            }
            if (a.is_active > b.is_active) {
                return -1;
            }
            return 0;
        })
    }







    const deleteUser = async (user) => {

        var answer = window.confirm("Confirma exclusão de " + user.name + " ?");
        if (answer) {

            try {
                await deleteUserById(user._id || user.id).catch(err => {
                });

                setAlert({
                    type: "success",
                    message: `Exclusão de ${user.name} efetuada `,
                    show: true,
                    simNao: false,
                })


                setUsers([])
                setTimeout(getList(), 3000)

            } catch (error) {
                alert('Houver um erro na deleção de usuário ', error)
            }
        }
    }


    const estilo = {
        cursor: 'pointer',
        textAlign: 'center'
    }



    //Este usereffet mostra o usuário logado 
    useEffect(() => {
        (async () => {
            const { user: loggedUser } = await jwt.decode(getToken())

            setUserFunction(loggedUser.funcao)

        })()
        return () => { }

    }, [])

    useEffect(() => {
        setUsers([])
        getList()
        return () => { }
    }, [listQuery])


    /**
    * atualiza a lista de Pessoas após a atualização
    */
    function refreshList() {
        setUsers([])
        getList()
    }
    const verifyIsEmpty = users.length === 0


    const inativarAtivar = async (user) => {

        var answer = window.confirm(`Confirma alteração do status do usuário ${user.name} ${user._id}?`)

        if (!answer) {
            return
        }

        const aluno = await patcByhRotaAndId("edit", (user._id || user.id), { is_active: !user.is_active })

        if (aluno) {

            refreshList()

            setAlert({
                type: "success",
                message: `O status de ${user.name} foi alterado com sucesso.`,
                show: true,
                simNao: false

            })

        }
    }

    //**retorna cabeçalho e a lista de user  */
    const cabecalhoLista = () => (

        <>

            <div className="list_user">

                <table className="table table-striped" border="1">
                    <thead>
                        <tr>
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> NOME </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> MATRICULA </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> ID </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> ATIVO </th> : ""}
                            {"ADM".includes(userFunction) ? <th> FUNÇÃO </th> : ""}
                            {"ADM".includes(userFunction) ? <th> TELEFONE </th> : ""}
                            {/* {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> AVALIACAO </th> : ""}
                            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <th> EXERCICIOS </th> : ""} */}
                            {"ADM_ALUNO".includes(userFunction) ? <th> VENCIMENTO MENSALIDADE </th> : ""}
                            {"PROFESSOR_ADM_ALUNO".includes(userFunction) ? <th> AÇÕES </th> : ""}

                        </tr>
                    </thead>
                    <tbody>
                        {montarUsuarios()}
                    </tbody>
                </table>
            </div>
        </>
    )

    const montarUsuarios = () => users.map((user, index) => (


        <tr key={index} className={user.is_active ? "" : "noActive"}>

            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <td> {user.name} </td> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <td> {user.matricula} </td> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <td> {user._id || user.id} </td> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <td> {user.is_active ? "SIM" : "NÃO"} </td> : ""}
            {"ADM".includes(userFunction) ? <td> {user.funcao} </td> : ""}
            {"ADM".includes(userFunction) ? <td> {user.telefone} </td> : ""}
            {/* {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <td> {user.avaliacao} </td> : ""}
            {"PROFESSOR_ESTAGIARIO_ADM_ALUNO".includes(userFunction) ? <td> {user.exercicios} </td> : ""} */}

            {"ADM_ALUNO".includes(userFunction) ? <td> {user.funcao.includes("ALUNO") ? user.mensalidade : ""} </td> : ""}

            {"PROFESSOR_ADM_ALUNO".includes(userFunction) ?

                <td style={estilo} >

                    {"ADM".includes(userFunction) ?
                        <>
                            <span onClick={() => inativarAtivar(user)} > <FaToggleOn />  Des/ativar</span> |
                            <span onClick={() => editUser(user)} > <FaPencilAlt />  Editar</span> |
                            <span onClick={() => deleteUser(user)}> <FaTrashAlt />  Excluir </span>
                        </>
                        : ""}

                    {"ALUNO".includes(userFunction) ?

                        <span > <IoIosFitness />  Série</span>
                        : ""}

                    {"PROFESSOR".includes(userFunction) ?
                        <span onClick={() => editExercicios(user)} > <FaPencilAlt /> Editar Série</span>
                        : ""}

                </td>

                : ""}

        </tr>


    )

    )

    return (
        <div>
            <MyModal
                show={alert.show || false}
                title={"Informe:"}
                funcaoFechar={() => setAlert({})}
                messagebody={alert.message || ""}
                simNao={alert.simNao}
                funcaoDesejada={alert.funcaodesejada}
            />

            <div className=''>
                <Container fluid className="centro listFiltros">
                     <FormGroup className='listFiltros centro '>
                        <Row>

                        <Col> <h2>Filtros:</h2>  </Col>
                            <Col>
                                <Button2 onClick={() => setListQuery({ funcao: "ALUNO" })} >Alunos </Button2>
                            </Col>
                            <Col>
                                <Button2 onClick={() => setListQuery({ funcao: "PROFESSOR" })}>Professor </Button2>
                            </Col>
                            <Col>
                                <Button2 onClick={() => setListQuery({ funcao: "ADM" })}> Administradores</Button2>
                            </Col>
                            <Col>
                                <Button2 onClick={() => setListQuery({ funcao: "ESTAGIARIO" })}> Estagiarios </Button2>
                            </Col>
                            <Col>
                                <Button2 onClick={() => setListQuery({})}> Sem_Filtro </Button2>
                            </Col>

                        </Row>
                    </FormGroup>
                </Container>
            </div>




            <Container fluid>
                {/* <Row className= "centro"> */}
                {!verifyIsEmpty ? cabecalhoLista() : <Loading show={loading} />}
                {/* </Row> */}

            </Container>
        </div>
    )

}




export default UserList


const Button2 = styled(Button)`
background-color: rgb(2, 4, 56);
border: solid 1px rgb(2, 4, 56);
color:rgb(95, 126, 128);
`