import React, { useEffect, useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import jwt from 'jsonwebtoken'
import { getToken } from '../../../config/auth';

function Navb(props) {
    const [collapsed, setCollapsed] = useState(true);
    const [userFunction, setUserFunction] = useState("doidera")
    const toggleNavbar = () => setCollapsed(!collapsed);

    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken())
            setUserFunction(user.funcao)
        })()
        return () => { }

    }, [])
    
    return (
        <div>
           <div className='od_hl'></div>
          <Navbar color="faded" dark>
            <NavbarBrand className="mr-auto ">Barra de Navegação</NavbarBrand>

            <NavbarToggler onClick={toggleNavbar} className="mr-2 orange_dark" />

            <Collapse isOpen={!collapsed} navbar>
              <Nav navbar>

                <NavItem>
                  <NavLink className='orange_dark2' href="/create">Cadastros</NavLink>
                </NavItem>

                <NavItem>
                  <NavLink className='orange_dark2' href="/list">Lista de Usuários</NavLink>
                </NavItem>

                {"PROFESSOR".includes(userFunction) ? 
                <NavItem>
                  <NavLink className='orange_dark2' href="/professorcreate/">Lista de exercícios </NavLink>
                </NavItem>
                 : ""}
              
              </Nav>
            </Collapse>
          </Navbar>
        </div>
      );
}

export default Navb;



