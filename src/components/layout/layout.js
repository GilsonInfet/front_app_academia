import React  from 'react';
import Nav from '../../components/layout/nav/nav'
import Header from './header/header'
import Footer from './footer/footer'
import { Container } from 'reactstrap';

const  Layout = (props) => {
     
    return (
        <Container fluid>
            <Header {...props} title="App Academia" />
            {props.showNav === true ?   <Nav {...props} name="Nova Página Inicial"/> : ""}
{/* className="conteudo centro" */}
            <main  >
                {props.children}
            </main>
            
            <Footer />
            
        </Container>
    )

}


export default Layout;
