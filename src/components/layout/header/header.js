import React from 'react'
import { useHistory } from 'react-router-dom'

import { removeToken } from '../../../config/auth'

import {FaDumbbell} from 'react-icons/fa'
import { MdExitToApp } from 'react-icons/md';

const Header = (props) => {

    const history = useHistory()

    const LogoutMe = () => {
        history.push("/login")
        removeToken()

    }



    return (
        <header className="oeste maxSpaceBet">
            <div className="title"> <FaDumbbell/> 
            &nbsp;&nbsp;&nbsp;
             {props.title}</div>
            {/* <button className='btn btn-outline-dark orange_dark' onClick={toggleuser} > Troca user </button>  */}

            <div className="profile">
                
                {props.info.name ? `${props.info.name}            ` : ""}
                {props.info.name ? <button className='btn btn-outline-dark orange_dark' onClick={LogoutMe} > <MdExitToApp /> Sair </button> :
                 <button className='btn btn-outline-dark orange_dark' onClick={LogoutMe} > <MdExitToApp /> Entrar </button>}
            </div>


        </header>
    )
}


export default Header
