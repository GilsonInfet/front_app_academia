import React from 'react';
import { Container, Row } from 'reactstrap';

function Footer() {
    return (
        <footer>
        <Container>


            <Row>
                <div className="bloco centro">
                <a href='https://reactstrap.github.io/components/layout/#app'>Suporte ao Usuário</a>

                 </div>

                <div className="bloco centro">
                <p>Site desenvolvido por <a href='https://gilsonpaulo.com.br/'> Gilson Paulo Web</a> </p>
                     
                </div>
                <div className="bloco centro"> Server desenvolvido por Lucas Pampulha Web Solutions.</div>
            </Row>


        </Container>
        </footer>
    );
}

export default Footer;
