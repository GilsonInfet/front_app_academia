

import React, { useState } from 'react';
import { userAuth } from '../../services/requisicoes'
import { useHistory } from 'react-router-dom';
import { saveToken } from '../../config/auth'
import { clientHttp } from '../../config/config'
import { Button  } from 'reactstrap';
import Layout from '../layout/layout'
import Alert from '../alert'
import GlobalStyle from '../../assets/styles/globalStyle';

const Login = (props) => {


    const [auth, setAuth] = useState({})
    const [useinfo, setuseinfo] = useState({})
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState("")
    const history = useHistory()


    const handleChange = (event) => {

        setAuth({
            ...auth,
            [event.target.name]: event.target.value
        })
        return;
    }



    const isValidSubmit = () => auth.matricula && auth.password

    /**Submete para login em evento click enter */
    const pressEnter = (event) => event.key === 'Enter' ? SubmitLogin() : null

    /**envia submit para login
     * 
     */
    const SubmitLogin = async () => {


        if (isValidSubmit()) {

            setLoading(true)

            await setTimeout(async () => {


                try {

                    const { data: { token, user } } = await userAuth(auth)

                    saveToken(token)

                    clientHttp.defaults.headers['x-auth-token'] = token;
                    history.push('/list')
                    setLoading(false)

                } catch (error) {
                    setLoading(false)


 
                    if (error.response) {
                        const erroCurrent = error.response.data.errors
                        const allItens = erroCurrent.map(item => item.msg)
                        const allItensToString = allItens.join('-')
                        setError(allItensToString)
                    } else {
                        setError("Não houve resposta do servidor.")
                    }

                }

                
            }, 1000)


        }
        return;
    }





    return (
        <Layout info={useinfo} > 

<GlobalStyle />
            <div className="form_login  allFill centro">

                <div className="sul ">
                    
                    <div className="input-group mb-3">

                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">Matrtícula</span>
                        </div>

                        <input className="form-control" onKeyPress={pressEnter} disabled={loading} type="text" id="matricula" name="matricula"
                            onChange={handleChange} value={auth.matricula || ""} placeholder="Insira sua matrícula" />
                        </div>

                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">Senha</span>
                        </div>
                        <input className="form-control" onKeyPress={pressEnter} disabled={loading} type="password" id="password" name="password"
                            onChange={handleChange} value={auth.password || ""} placeholder="Insira sua senha" />

                    </div>


                    <Button className="orange_dark" size="lg" block disabled={!isValidSubmit()} onClick={SubmitLogin} >
                        {loading ? (<i className="fa fa-spinner fa-spin fa-3x fa-fw">   </i>) : "Entrar"}
                    </Button>

                    <div className="alertLogin">
                        <Alert show={error ? true : false} type="error" message={error} />
                    </div>

                </div>
            </div>

        </Layout>
    )
}


export default Login;
