import React from 'react';
import { Button, Modal } from 'react-bootstrap'

/**
 * props show - true exibe o modal, //
 * title - texto do abeçalho, //
 * funcaoFechar - função sue seta false controlador de exibir, //
 * messagebody - mensagem do corpo do modal, //
 * simnao = true botão cancelar e botão sim:continuar
 * funcaoDesejada - função executada em caso de ok
 * @param {*} props 
 */
function MyModal(props) {
    return (
        <>
            <Modal show={props.show} onHide={props.funcaoFechar}>
                <Modal.Header closeButton>
                    <Modal.Title>{props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>{props.messagebody}</Modal.Body>
                <Modal.Footer>

                    {props.simNao === true ?
                        (<> <Button variant="secondary" onClick={props.funcaoFechar}> NÃO </Button>
                          <Button variant="primary" onClick={props.funcaoDesejada}> SIM </Button> </>)
                        :
                        <Button variant="primary" onClick={props.funcaoFechar}> OK </Button>
                    }
                </Modal.Footer>
            </Modal>
        </>
    );

}

export default MyModal
