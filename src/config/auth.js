const TOKEN_KEY = 'x-auth-token' //'bootcamp-infnet'

/**
 * from auth.js, retorna localStorage.getItem(TOKEN_KEY)
 */
const getToken = () => localStorage.getItem(TOKEN_KEY)

/**
 * Salva o token em localstorage
 * @param {token key} token 
 */
const saveToken = (token) => localStorage.setItem(TOKEN_KEY, token)

/**
 * Remove o token do local Storage
 */
const removeToken = () => localStorage.removeItem(TOKEN_KEY)


const isAuthenticated = () => {

    if (getToken()){
        return true
    }
    return getToken() !== null
}


export {
    isAuthenticated,
    getToken,
    saveToken,
    removeToken
}
