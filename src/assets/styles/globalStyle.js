


import 'bootstrap/dist/css/bootstrap.min.css';
import  { createGlobalStyle } from 'styled-components';
import academiapesos from  '../img/fitness-594143_1920.jpg' 

const GlobalStyle = createGlobalStyle`
 * { 
    
    margin:0px;
    padding:0px;
    outline:0;
    -webkit-font-smoothing: antialiased;

   
    body {
        background-image: url(${academiapesos});
    }
  }
`;
 

export default GlobalStyle