import React, { useEffect, useState } from 'react'
import Layout from '../components/layout/layout'
import UserList from '../components/user/list'
import UserCreate from '../components/user/create'
import ProfessorForm from '../components/user/professorForm'
import ProfessorFormCreate from '../components/user/professorFormCreate'
import Aluno from '../components/user/aluno'
import ProfessorList from '../components/user/professorlist'
import jwt from 'jsonwebtoken'
import { useHistory } from 'react-router-dom';

import { getToken } from '../config/auth';

import { Route, Switch, Redirect } from "react-router-dom";

const ViewUser = (props) => {

    const history = useHistory()

    const [useinfo, setuseinfo] = useState({})
     const [showNavBar, setShowNavBar] = useState(true)


    useEffect(() => {

        (async () => {

            try {
                const { user } = await jwt.decode(getToken())
                setuseinfo(user) 
            } catch (error) {

                 history.push("/login")
            }

        })()

        return () => { }
    }, [])



    const AdminRoute = ({ ...rest }) => {

 
        if ("ALUNO".includes(useinfo.funcao) && ! rest.aluno) {
            setShowNavBar(false)
            return <Redirect to="aluno" />    
        } else if ("PROFESSOR".includes(useinfo.funcao) && !rest.professor){
            return <Redirect to="professorlist" />            
        } else if ("ESTAGIARIO".includes(useinfo.funcao) && !rest.estagiario){
            setShowNavBar(false)
            return <Redirect to="estagiariolist" />
        } else if ("ADM".includes(useinfo.funcao) && !rest.admin) {
                return <Redirect to="/403" />
        }else{
            return <Route {...rest} basename={props.match.path} />
        }

        
    }


    return (
        <Layout info={useinfo} showNav={showNavBar}>

            <Switch>

                <AdminRoute admin exact path={props.match.path + "edit/:id"} component={UserCreate} />               
                <AdminRoute admin exact path={props.match.path + "create"} component={UserCreate} />
                <AdminRoute admin exact path={props.match.path + "list"} >
                    <UserList info={useinfo} />
                </AdminRoute>

                <AdminRoute professor exact path={props.match.path + "editexercicios/:id"} component={ProfessorForm} />
                <AdminRoute professor exact path={props.match.path + "professorcreate"} component={ProfessorFormCreate} />
                <AdminRoute professor exact path={props.match.path + "professorlist"} >
                    <ProfessorList info={useinfo} />
                </AdminRoute>                


                <AdminRoute estagiario exact path={props.match.path + "estagiariolist"} >
                    <ProfessorList info={useinfo} />
                </AdminRoute>   

                <AdminRoute aluno exact path={props.match.path + "aluno"} >
                    <Aluno info={useinfo} />
                </AdminRoute>

                <Route exact path={"/403"} component={() => <h1> Área de acesso restrito. </h1>} />
                <Route exact path={"/"} component={() => <h3> {useinfo.name},  Bem vindo ao App Academia. Utilize a barra ou menu  de navegação para acessar as views de seu perfil.</h3>} />

            </Switch>
        </Layout>
    )
}

export default ViewUser





