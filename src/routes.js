import React from 'react'
import Login from './components/login/login'
import ViewUser from './views/User';
import { isAuthenticated } from './config/auth';

import ErrorHandler from './views/errors/error';
import history from '../src/config/history'
import {  Switch, Route, Redirect, Router } from "react-router-dom";



const CustomRoute = ({ ...rest }) => {

    if (!isAuthenticated()) {
       
        return <Redirect to='/login' />

    }
    return <Route  {...rest} />
}



const Routers = (props) => {

    return (
        <Router history={history} >
            
            <Switch>
            
                <Route exact path="/login" component={Login} />
                <Route exact path="/erro/:erro" component={ErrorHandler} />
                <CustomRoute path="/" component={ViewUser} /> 
                
            </Switch>

        </Router>
    )
}

export default Routers


