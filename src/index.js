import React from 'react';
import ReactDOM from 'react-dom';

import Routers from './routes';
import 'bootstrap/dist/css/bootstrap.min.css';
import './global.css';



ReactDOM.render(

  <React.StrictMode>
     
    <Routers />
    
  </React.StrictMode>,
  document.getElementById('root')
);
